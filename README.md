# IDS721 Project 2

## The Link to My Demo Video
- [**Check My Demo Video**](https://youtu.be/qcxyHN3P0WI)

## Create Rust Web Service
Follow the Week4's guide to create a simple web service.
1. Use `cargo new ids721-project2` to create a new binary-based Cargo project
2. Add required dependencies for this project in `Cargo.toml`
```toml
[dependencies]
actix-web = "4"
serde = { version = "1.0.196", features = ["derive"]}
```
3. Start a web server following the instructions on [Getting Started](https://actix.rs/docs/getting-started)
4. Create greedy coin change algorithm in the new created file `lib.rs` under `src`
5. update the `main.rs` to import the algorithm in `lib.rs` as `use ids721_project2::greedy_coin_change;`
6. Start the web service with `cargo run` and test the function with `cargo test`
7. Go to `localhost:8080` and test the web service
- <img src="img/webapplication.png" style="width:400px;">

## Containerize the Web Service
1. Create `Dockerfile` in the application's root directory
```Dockerfile
#Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

#Distroless runtime stage
FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/ids721-project2 /app/

#Use non-root user
USER nonroot:nonroot

#Set up app directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

#Expose port
EXPOSE 8080

#Run the app
ENTRYPOINT ["/app/ids721-project2"]
```
2. To check if the Dockerfile is correct, we can build the Docker image with `docker build -t ids721-project2 .` locally, and start a Docker container with `docker run -p 8080:8080 ids721-project2`
   

### Configure AWS ECR and Permissions
AWS Elastic Container Registry (ECR) is a fully managed Docker container registry provided by Amazon Web Services (AWS) that makes it easy for developers to store, manage, and deploy Docker container images. To push the image to an AWS ECR repository, we should:

1. Create a new repository on AWS ECR called idsproject2

![Repository](img/repo.png)

2. View the push command of the repository, mark down the URI of the repository

![PushCommand](img/push.png)

3. Attach required policy for interacting with AWS ECR to the user we will be using

![Policy](img/policy.png)

4. Generate a new access keys under the user's security credentials, save the access key and secret access key

![AccessKey](img/keys.png)

### Write Configuration File for Gitlab Pipeline

In the project's Gitlab repository, go to Settings -> CI/CD -> Variables, create new variables with information from AWS
   
![CIVariables](img/variables.png)   

### Create .gitlab-ci.yml File to Create Workflow
Create a yml file called `.gitlab-ci.yml` in the project:
```yaml
stages:
  - build
  - deploy

variables:
  AWS_ECR_REGISTRY: $AWS_ECR_REGISTRY
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION


build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - apk update && apk add --no-cache python3 py3-pip
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install awscli
    - aws --version
  script:
    - docker build -t idsproject2 .
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set default.region $AWS_DEFAULT_REGION
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $AWS_ECR_REGISTRY
    - docker tag idsproject2:latest $AWS_ECR_REGISTRY/idsproject2:latest
    - docker push $AWS_ECR_REGISTRY/idsproject2:latest

deploy:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - apk update && apk add --no-cache python3 py3-pip
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install awscli
    - aws --version
  script:
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set default.region $AWS_DEFAULT_REGION
    - aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $AWS_ECR_REGISTRY
    - docker pull $AWS_ECR_REGISTRY/idsproject2:latest
    - docker run -d -p 8080:8080 $AWS_ECR_REGISTRY/idsproject2:latest
    - sleep 10
    - docker ps
```

This configuration containerize the microservice and push it to AWS ECR, then pull it and start a container to check if it's working.

### Push and Pull the Service to AWS ECR
As soon as we push the `.yml` file to Gitlab repository, the CI/CD will automatically detect the file and run the pipeline. We can check at the AWS ECR repository to see if the image is correctly pushed and stored.

![ImageECR](img/ecr.png)

The job page shows that the image is correctly pulled and the container is running.
![RunningContainer](img/dockerrunning.png)
