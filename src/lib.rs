//Greedy coin change logic

pub fn greedy_coin_change(amount: u32) ->Vec<u32>{
    let mut coins = vec![1, 5, 10, 25];
    coins.sort();
    coins.reverse();

    let mut remaining = amount;
    let mut result = vec![];

    for coin in coins{
        while remaining >= coin{
            remaining -= coin;
            result.push(coin);
        }
    }

    result
}

//test
#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn test_greedy_coin_change(){
        assert_eq!(greedy_coin_change(1), vec![1]);
        assert_eq!(greedy_coin_change(5), vec![5]);
        assert_eq!(greedy_coin_change(10), vec![10]);
        assert_eq!(greedy_coin_change(25), vec![25]);
        assert_eq!(greedy_coin_change(6), vec![5, 1]);
        assert_eq!(greedy_coin_change(7), vec![5, 1, 1]);
        assert_eq!(greedy_coin_change(8), vec![5, 1, 1, 1]);
        assert_eq!(greedy_coin_change(9), vec![5, 1, 1, 1, 1]);
        assert_eq!(greedy_coin_change(11), vec![10, 1]);
        assert_eq!(greedy_coin_change(12), vec![10, 1, 1]);
        assert_eq!(greedy_coin_change(13), vec![10, 1, 1, 1]);
        assert_eq!(greedy_coin_change(14), vec![10, 1, 1, 1, 1]);
        assert_eq!(greedy_coin_change(15), vec![10, 5]);
        assert_eq!(greedy_coin_change(16), vec![10, 5, 1]);
        assert_eq!(greedy_coin_change(17), vec![10, 5, 1, 1]);
        assert_eq!(greedy_coin_change(18), vec![10, 5, 1, 1, 1]);
        assert_eq!(greedy_coin_change(19), vec![10, 5, 1, 1, 1, 1]);
        assert_eq!(greedy_coin_change(20), vec![10, 10]);
        assert_eq!(greedy_coin_change(21), vec![10, 10, 1]);
        assert_eq!(greedy_coin_change(22), vec![10, 10, 1, 1]);
        assert_eq!(greedy_coin_change(23), vec![10, 10, 1, 1, 1]);
        assert_eq!(greedy_coin_change(24), vec![10, 10, 1, 1, 1, 1]);
        assert_eq!(greedy_coin_change(26), vec![25, 1]);
    }
}